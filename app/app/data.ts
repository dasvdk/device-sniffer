import { hostname, networkInterfaces, NetworkInterfaceInfo } from 'os'

import { Interface, Device } from './models'
import { IInterface as IInterface, IDevice } from './interfaces'
import { NmapScan } from 'node-nmap'

export class Data { 
    constructor() {
    }
    
    getHostname() : Promise<string> {
        return Promise.resolve(hostname())
    } 
    
    getInterfaces() : Promise<IInterface[]> {
        return new Promise((resolve, reject) => {
            try {
                let nis = networkInterfaces()
                let interfaces : Interface[] = []
                Object.keys(nis).map(key => {
                    nis[key].forEach(_if => {
                        interfaces.push(new Interface(key, _if.mac, _if.cidr, _if.family))
                    })
                })
                resolve(interfaces)
            } catch (e) {
                reject(e)
            }
        })
    }

    getScanResults(family: string, cidr: string) : Promise<IDevice[]> {
        let args = family === "IPv6" ? "-6" : ""
        let scanner = new NmapScan(cidr, "-sP " + args)
        return new Promise<IDevice[]>((resolve, reject) => {
            try {
                scanner.on('complete', res => {
                    if (res) {
                        var devices = res.filter(r => r.ip && r.mac).map(r => new Device(r.ip, r.mac, r.hostname || r.vendor))
                        if (!devices || devices.length === 0){
                            resolve([])
                        } else {
                            resolve(devices)    
                        }
                    } else {
                        resolve([])
                    }
                })
                scanner.startScan();
            } catch (e) {
                reject(e)
            }
        })
    }
}