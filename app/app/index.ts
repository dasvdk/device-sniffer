import { observable } from 'knockout'
import * as ipc from 'electron-promise-ipc'

class IndexModel {
    hostname: KnockoutObservable<string>
    constructor(hostname: string) {
        this.hostname = observable(hostname);
    }

    getHostname(): void {
        ipc.send('getHostname')
            .then(hostname => this.hostname(hostname))
    }

    openScanner(): void {
        ipc.send('openScanner');
    }

}

export { IndexModel }