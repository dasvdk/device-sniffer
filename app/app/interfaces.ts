
export interface IInterface {
    name: string
    mac: string
    family: string
    cidr: string
    devices: IDevice[]
}

export interface IDevice {
    ip: string
    mac: string
    hostname: string
}
