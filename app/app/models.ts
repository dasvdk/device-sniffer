import { IInterface, IDevice } from './interfaces'

export class Interface implements IInterface {
    name: string
    mac: string
    cidr: string
    family: string
    devices: IDevice[]
    
    constructor(name: string, mac: string, cidr: string, family: string, devices: IDevice[] = []) {
        this.name = name
        this.mac = mac
        this.cidr = cidr
        this.family = family
        this.devices = devices
    }
}

export class Device implements IDevice {
    ip: string
    mac: string
    hostname: string

    constructor(ip: string, mac: string, hostname: string) {
        this.ip = ip
        this.mac = mac
        this.hostname = hostname
    }
}