const ko = require('knockout')

import * as ipc from 'electron-promise-ipc'
import { IInterface, IDevice } from './interfaces'
import { Interface } from './models'

class ScannerModel {
    working: KnockoutObservable<boolean>
    interfaces: KnockoutObservableArray<InterfaceModel>
    
    constructor() {
        this.interfaces = ko.observableArray([])
        this.working = ko.observable(false)
    }

    getInterfaces(): void {
        this.working(true)
        ipc.send('getInterfaces')
            .then((ifs : IInterface[]) => 
                this.interfaces(ifs.map(_if => new InterfaceModel(_if.name, _if.cidr, _if.family, []))), console.log("finished 1")
            ).catch(err => 
                console.error(err)
            ).finally(_ => {
                this.working(false)
            })
    }

}

class InterfaceModel {
    name: KnockoutObservable<string>
    cidr: KnockoutObservable<string>
    family: KnockoutObservable<string>
    devices: KnockoutObservableArray<DeviceResultModel>
    working: KnockoutObservable<boolean>

    constructor(name: string, cidr: string, family: string, devices: DeviceResultModel[]) {
        this.name = ko.observable(name)
        this.cidr = ko.observable(cidr)
        this.family = ko.observable(family)
        this.devices = ko.observableArray(devices)
        this.working = ko.observable(false)
    }

    getScanResults(): void {
        this.working(true)
        ipc.send('getScanResults', { cidr: this.cidr(), family: this.family() })
            .then((devices : IDevice[]) => {
                this.devices(devices.map(d => new DeviceResultModel(d.ip, d.mac, d.hostname))), console.log("finished 2")
            }).catch( err => {
                console.error(err)
            }).finally(_ =>
                this.working(false)
            )
    }
}

class DeviceResultModel {
    ip: KnockoutObservable<string>;
    mac: KnockoutObservable<string>;
    hostname: KnockoutObservable<string>;

    constructor(ip: string, mac: string, hostname: string) {
        this.ip = ko.observable(ip)
        this.mac = ko.observable(mac)
        this.hostname = ko.observable(hostname)
    }
} 

export { ScannerModel }

