export { }

import * as ipc from 'electron-promise-ipc'
import { Data } from './app/data'
import { createWindows, getWindow } from './main'

ipc.on('getHostname', _ => {
  return new Data().getHostname()
})

ipc.on('openScanner', _ => {
  createWindows('scanner', 'app/scanner.html')
})

ipc.on('getInterfaces', _ => {
  return new Data().getInterfaces()
})

ipc.on('getScanResults', data => {
  return new Data().getScanResults(data.family, data.cidr)
})