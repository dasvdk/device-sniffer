// Modules to control application life and create native browser window
import { app, BrowserWindow } from 'electron'
import './communication'

const windows : WindowDescriptor[] = []
export function getWindow(name: string) : BrowserWindow {
  for (let i = 0; i < windows.length; i++ ){
    if (windows[i].name === name) {
      return windows[i].window;
    }
  }
  return null;
}

export function createWindows (name, path) {
  [1].forEach(_ => {

    let win = new BrowserWindow({width: 800, height: 600})

    // and load the index.html of the app.
    win.loadFile(path)

    // Open the DevTools.
    win.webContents.openDevTools()
    
    // Emitted when the window is closed.
    win.on('closed', (evt) => { 
      // Dereference the window object, usually you would store windows
      // in an array if your app supports multi windows, this is the time
      // when you should delete the corresponding element.
      for (let i = 0; i < windows.length; i++)
      {
        if (windows[i].window === evt.sender) {
          windows.splice(i, 1);
        }
      }
    })
    
    windows.push(new WindowDescriptor(name, win));
  });
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', _ => createWindows('index', 'app/index.html'))

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function (win) {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) {
    createWindows('index', 'app/index.html')
  }
})

class WindowDescriptor {
  window: BrowserWindow;
  name: string;
  constructor(name: string, window: BrowserWindow) {
    this.name = name;
    this.window = window;
  }
}