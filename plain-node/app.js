'use strict';

var ping = require('ping');
var os = require('os');
var nmap = require('node-nmap');
var fs = require('fs');

var ifaces = os.networkInterfaces();
function getInterfaces() {
  var ifs = [];
  Object.keys(ifaces).forEach(function (ifname) {
    var alias = 0;
    ifaces[ifname].forEach(function (iface) {
      if ('IPv4' !== iface.family || iface.internal !== false) {
        // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
        return;
      }

      if (alias >= 1) {
        // ignore, cuz same address
      } else {
        // this interface has only one ipv4 adress
        ifs.push(iface.cidr);
      }
      ++alias;
    });
  });
  return ifs;
}

nmap.nmapLocation = "nmap"; //default
function pingAll(cb) {
  console.log("Trying to scan interfaces");

  var ifs = getInterfaces();
  var numCompleted = 0;
  var totalScans = ifs.length;
  var ipMacMap = {};

  function initializeQuickScan(qc) {
    qc.on('complete', function(data){
      data.filter(d => d.mac).forEach(d => {
        ipMacMap[d.mac] = d.ip;
      });
      numCompleted++;

      console.log("Completed scan " + numCompleted + "/" + totalScans);
      if (numCompleted === totalScans) {
        save(ipMacMap, cb);
      }
    });
    qc.on('error', function(error){
      cb(error, null);
    });
    return qc;
  }

  var scans = ifs.map((iface) => {
    console.log("Creating scanner for interface: " + iface);
    return { iface: iface, quickscan: initializeQuickScan(new nmap.QuickScan(iface))};
  });
  
  scans.forEach(qc => {
    console.log("Scanning interface " + JSON.stringify(qc.iface));
    qc.quickscan.startScan();
  });
}

var filename = 'macmap.json';
function save(ipMacMap, cb, override) {
  fs.writeFile(filename, JSON.stringify(ipMacMap), 'utf8', cb);
}
function load(cb) {
  fs.readFile(filename, 'utf8', cb);
}

function getIp(mac, cb) {
  load((err, data) => {
    if (err) {
      console.warn("Could not open mapping file");
      cb(err, null);
    } else {
      cb(null, JSON.parse(data)[mac]);
    }
  });
}

function pingIp(ip, cb) {
  console.log("Pinging IP: " + ip);
  ping.sys.probe(ip, response => cb(null, response)) 
}

function pingMac(mac, cb) {
  console.log("Pinging MAC: " + mac);
  getIp(mac, (err, ip) => {
    if (err) {
      cb({ message: "Couldn't find MAC on interfaces", mac: mac }, null);
    } else {
      pingIp(ip, cb);
    }
  });
}

var retry = true;
function responseFunc(err, isAlive) {
  if (err) {
    console.log(err.message);
    pingAll((err2, data) => {
      if (err2) {
        console.log(err2);
      } else {
        if (retry) {
          retry = false;
          pingMac(err.mac, responseFunc);
        } else {
          console.log("Could not find device");
        }
      }
      return;
    });
  } else {
    console.log("Device is " + (isAlive ? "" : "_not_ ") + "online");
  }
};
pingMac(process.argv[2] || "B8:85:84:B0:2B:BF", responseFunc);

